# Ceska sporitelna - Job interview project

## App description

Simple app which returns information about branch and weather at branch location.

Application contains swagger UI (/webjars/swagger-ui/index.html#/) with REST api description. Application also contains
actuator for health checks.

## Environment variable
CS_PLACES_TOKEN - Ceska sporitelna api needs token for production env. For mock server (which returns always same data) you can 
use dd3c4d08-6c39-411a-a37d-0fbea365fc1e [official documentation](https://developers.erstegroup.com/docs/apis/bank.csas/bank.csas.v3%2Fplaces).

WEATHER_TOKEN - [documentation](https://openweathermap.org/api)


## Ideas for improvements 
- Spring security - Due to a lack of knowledge, spring security is not implemented
- Branch cache - Implement a branch cache that will be flushed once an hour. @Cachable is probably not compatible with Spring reactor.
- Dynamic properties for wire mock - Port is hardcoded to local host 8081.