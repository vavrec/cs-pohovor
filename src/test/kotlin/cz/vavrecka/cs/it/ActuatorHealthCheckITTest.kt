package cz.vavrecka.cs.it

import org.junit.jupiter.api.Test

class ActuatorHealthCheckITTest : AbstractTest() {

    @Test
    fun `actuator healthcheck returns 200`() {
        // simple test that checks if the actuator is running
        webTestClient.get()
            .uri("/actuator/health")
            .exchange()
            .expectStatus()
            .isOk
    }
}