package cz.vavrecka.cs.it

import com.github.tomakehurst.wiremock.client.WireMock
import cz.vavrecka.cs.client.exception.ClientException
import cz.vavrecka.cs.client.exception.ClientServerException
import cz.vavrecka.cs.client.model.weather.CurrentDto
import cz.vavrecka.cs.client.model.weather.WeatherDto
import cz.vavrecka.cs.client.model.weather.WeatherErrorResponse
import cz.vavrecka.cs.client.model.weather.WeatherResponseDto
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType

class WeatherClientItTest : AbstractTest() {

    @Test
    fun `response ok`() {
        runBlocking {
            val weather = WeatherResponseDto(CurrentDto(listOf(WeatherDto(description = "slunecno"))))
            wireMockServer.stubFor(
                WireMock.get(WireMock.urlMatching("/data/3.0/onecall\\?lon=0.1&lat=0.1&appid=test&lang=cz"))
                    .willReturn(
                        WireMock.aResponse()
                            .withStatus(200)
                            .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                            .withBody(objectMapper.writeValueAsString(weather))
                    )
            )

            Assertions.assertThat(weatherClient.getCurrentWeatherByLocation(0.1, 0.1)).isEqualTo(weather)
        }
    }

    @CsvSource(
        " 0.1 , 0.1, 400",
        " 0.2 , 0.2, 401",
        " 0.3 , 0.3, 404"
    )
    @ParameterizedTest(name = ParameterizedTest.DISPLAY_NAME_PLACEHOLDER + " " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    fun `4xx errors`(lon: Double, lat: Double, httpStatus: String) {
        wireMockServer.stubFor(
            WireMock.get(WireMock.urlMatching("/data/3.0/onecall\\?lon=$lon&lat=$lat&appid=test&lang=cz"))
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(httpStatus.toInt())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(objectMapper.writeValueAsString(WeatherErrorResponse(httpStatus, "cz-13")))
                )
        )

        val message = assertThrows(ClientException::class.java) {
            runBlocking {
                weatherClient.getCurrentWeatherByLocation(lon, lat)
            }
        }.message

        Assertions.assertThat(message).contains("Weather api returns $httpStatus")
    }

    @CsvSource(
        " 0.1 , 0.1, 500",
        " 0.2 , 0.2, 501",
        " 0.3 , 0.3, 502"
    )
    @ParameterizedTest(name = ParameterizedTest.DISPLAY_NAME_PLACEHOLDER + " " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    fun `5xx errors`(lon: Double, lat: Double, httpStatus: String) {
        wireMockServer.stubFor(
            WireMock.get(WireMock.urlMatching("/data/3.0/onecall\\?lon=$lon&lat=$lat&appid=test&lang=cz"))
                .willReturn(
                    WireMock.aResponse()
                        .withStatus(httpStatus.toInt())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(objectMapper.writeValueAsString(WeatherErrorResponse(httpStatus, "cz-13")))
                )
        )

        val message = assertThrows(ClientServerException::class.java) {
            runBlocking {
                weatherClient.getCurrentWeatherByLocation(lon, lat)
            }
        }.message
        Assertions.assertThat(message).contains("Weather api failed to respond, after max attempts of")
    }
}