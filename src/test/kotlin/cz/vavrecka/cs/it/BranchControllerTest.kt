package cz.vavrecka.cs.it

import cz.vavrecka.cs.client.exception.ClientException
import cz.vavrecka.cs.client.exception.ClientServerException
import cz.vavrecka.cs.client.model.branch.BranchResponseDto
import cz.vavrecka.cs.client.model.branch.LocationDto
import cz.vavrecka.cs.client.model.weather.CurrentDto
import cz.vavrecka.cs.client.model.weather.WeatherDto
import cz.vavrecka.cs.client.model.weather.WeatherResponseDto
import cz.vavrecka.cs.controller.BRANCH_PATH
import cz.vavrecka.cs.controller.ErrorResponse
import cz.vavrecka.cs.model.BranchBasicInfoDto
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.Mockito.doReturn
import org.mockito.Mockito.doThrow
import java.util.*

class BranchControllerTest : AbstractTest() {

    @Test
    fun `response ok - get branch by id with weather`() {
        runBlocking {
            val lon = 55.00
            val lat = 20.00
            val id = 1L

            val weatherResponseDto = WeatherResponseDto(CurrentDto(listOf(WeatherDto("prsii"))))
            val branchResponseDto = BranchResponseDto(name = "test", address = "test address", city = "gotham", postCode = "19000", "Vse je ok", location = LocationDto(lat = lat, lng = lon))
            val expectedResponse = BranchBasicInfoDto(name = "test", address = "test address", city = "gotham", postCode = "19000", "Vse je ok", listOf("prsii"))

            doReturn(weatherResponseDto).`when`(weatherClient).getCurrentWeatherByLocation(lon = lon, lat = lat)
            doReturn(branchResponseDto).`when`(placeClient).getBranchById(id)

            webTestClient.get()
                .uri("$BRANCH_PATH/{id}", id)
                .exchange()
                .expectStatus()
                .isOk
                .expectBody(BranchBasicInfoDto::class.java)
                .isEqualTo(expectedResponse)
        }
    }


    @Test
    fun `bad request (advice test) - cs api, get branch by id`() {
        runBlocking {
            val exceptionId = UUID.randomUUID()
            val exceptionMessage = "Test message"

            doThrow(ClientException(exceptionMessage, exceptionId)).`when`(placeClient).getBranchById(Mockito.anyLong())

            // Client Exception test
            webTestClient.get()
                .uri("$BRANCH_PATH/{id}", 1L)
                .exchange()
                .expectStatus()
                .isBadRequest
                .expectBody(ErrorResponse::class.java)
                .isEqualTo(ErrorResponse(exceptionId, exceptionMessage))


            // Client Server Exception test
            doThrow(ClientServerException(exceptionMessage, exceptionId)).`when`(placeClient).getBranchById(Mockito.anyLong())
            webTestClient.get()
                .uri("$BRANCH_PATH/{id}", 11L)
                .exchange()
                .expectStatus()
                .isBadRequest
                .expectBody(ErrorResponse::class.java)
                .isEqualTo(ErrorResponse(exceptionId, exceptionMessage))
        }
    }

    @Test
    fun `bad request (advice test) - get current weather`() {
        runBlocking {
            val exceptionId = UUID.randomUUID()
            val exceptionMessage = "Test message bad request"

            val branchResponseDto = BranchResponseDto(name = "test", address = "test address", city = "gotham", postCode = "19000", "Vse je ok", location = LocationDto(lat = 11.1, lng = 11.1))

            doReturn(branchResponseDto).`when`(placeClient).getBranchById(Mockito.anyLong())
            doThrow(ClientException(exceptionMessage, exceptionId)).`when`(placeClient).getBranchById(Mockito.anyLong())

            // Client Exception test
            webTestClient.get()
                .uri("$BRANCH_PATH/{id}", 1L)
                .exchange()
                .expectStatus()
                .isBadRequest
                .expectBody(ErrorResponse::class.java)
                .isEqualTo(ErrorResponse(exceptionId, exceptionMessage))

            // Client Server Exception test
            doThrow(ClientServerException(exceptionMessage, exceptionId)).`when`(placeClient).getBranchById(Mockito.anyLong())
            webTestClient.get()
                .uri("$BRANCH_PATH/{id}", 2L)
                .exchange()
                .expectStatus()
                .isBadRequest
                .expectBody(ErrorResponse::class.java)
                .isEqualTo(ErrorResponse(exceptionId, exceptionMessage))
        }
    }
}