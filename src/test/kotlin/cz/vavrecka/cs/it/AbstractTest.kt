package cz.vavrecka.cs.it

import com.fasterxml.jackson.databind.ObjectMapper
import com.github.tomakehurst.wiremock.WireMockServer
import cz.vavrecka.cs.client.PlaceClient
import cz.vavrecka.cs.client.WeatherClient
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.SpyBean
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWireMock(port = 8081)
@AutoConfigureWebTestClient
abstract class AbstractTest {

    @Autowired
    lateinit var wireMockServer: WireMockServer

    @SpyBean
    lateinit var weatherClient: WeatherClient

    @SpyBean
    lateinit var placeClient: PlaceClient

    @Autowired
    lateinit var webTestClient: WebTestClient

    @Autowired
    lateinit var objectMapper : ObjectMapper

    @BeforeEach
    fun beforeEachTest(){
        wireMockServer.resetAll()
    }
}