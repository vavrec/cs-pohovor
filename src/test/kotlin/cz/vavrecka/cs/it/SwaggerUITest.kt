package cz.vavrecka.cs.it

import org.junit.jupiter.api.Test

class SwaggerUITest : AbstractTest() {

    @Test
    fun `swagger UI returns 200`() {
        // simple test that checks if the swagger UI is running
        webTestClient.get()
            .uri("/webjars/swagger-ui/index.html#")
            .exchange()
            .expectStatus()
            .isOk
    }
}