package cz.vavrecka.cs.it

import com.github.tomakehurst.wiremock.client.WireMock.*
import cz.vavrecka.cs.client.exception.ClientException
import cz.vavrecka.cs.client.exception.ClientServerException
import cz.vavrecka.cs.client.model.branch.BranchResponseDto
import cz.vavrecka.cs.client.model.branch.CsClientErrorResponseDto
import cz.vavrecka.cs.client.model.branch.LocationDto
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.CsvSource
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType

class PlaceClientItTest : AbstractTest() {

    @Test
    fun `response ok`() {
        runBlocking {
            val branchResponseDto = BranchResponseDto(name = "test", address = "test address", city = "gotham", postCode = "19000", "Vse je ok", location = LocationDto(lat = 11.1, lng = 11.1))
            wireMockServer.stubFor(
                get(urlMatching("/branches/0"))
                    .willReturn(
                        aResponse()
                            .withStatus(200)
                            .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                            .withBody(objectMapper.writeValueAsString(branchResponseDto))
                    )
            )
            assertThat(placeClient.getBranchById(0L)).isEqualTo(branchResponseDto)
        }
    }

    @CsvSource(
        " 1, 400",
        " 2, 401",
        " 3, 404"
    )
    @ParameterizedTest(name = ParameterizedTest.DISPLAY_NAME_PLACEHOLDER + " " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    fun `4xx errors`(branchId : Long, httpStatus: String) {
        wireMockServer.stubFor(
            get(urlMatching("/branches/$branchId"))
                .willReturn(
                    aResponse()
                        .withStatus(httpStatus.toInt())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(objectMapper.writeValueAsString(CsClientErrorResponseDto(httpStatus, emptyList(), "cz-13")))
                )
        )

        val message = assertThrows(ClientException::class.java) {
            runBlocking {
                placeClient.getBranchById(branchId)
            }
        }

        assertThat(message.message).isEqualTo("Ceska sporitelna api returns $httpStatus. More information can be found in the log." )
    }

    @CsvSource(
        " 1, 500",
        " 2, 501",
        " 3, 502"
    )
    @ParameterizedTest(name = ParameterizedTest.DISPLAY_NAME_PLACEHOLDER + " " + ParameterizedTest.DEFAULT_DISPLAY_NAME)
    fun `5xx errors`(branchId : Long, httpStatus: String) {

        wireMockServer.stubFor(
            get(urlMatching("/branches/$branchId"))
                .willReturn(
                    aResponse()
                        .withStatus(httpStatus.toInt())
                        .withHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                        .withBody(objectMapper.writeValueAsString(CsClientErrorResponseDto(httpStatus, emptyList(), "cz-13")))
                )
        )

        val message = assertThrows(ClientServerException::class.java) {
            runBlocking {
                placeClient.getBranchById(branchId)
            }
        }.message
        assertThat(message).contains("Places api failed to respond, after max attempts of")
    }
}