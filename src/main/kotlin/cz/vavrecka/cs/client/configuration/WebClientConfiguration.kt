package cz.vavrecka.cs.client.configuration

import cz.vavrecka.cs.client.exception.ClientException
import cz.vavrecka.cs.client.exception.ClientServerException
import cz.vavrecka.cs.client.model.branch.CsClientErrorResponseDto
import cz.vavrecka.cs.client.model.weather.WeatherErrorResponse
import cz.vavrecka.cs.client.properties.ClientProperties
import mu.KotlinLogging
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.reactive.function.client.ClientResponse
import org.springframework.web.reactive.function.client.ExchangeFilterFunction
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono
import java.util.*


private val logger = KotlinLogging.logger {}

@Configuration
class WebClientConfiguration {

    @Bean
    fun placesWebClient(builder: WebClient.Builder, props: ClientProperties): WebClient {
        return builder
            .baseUrl(props.csPlacesUrl)
            .defaultHeader("WEB-API-key", props.csPlacesApiKey)
            .filter(basicErrorHandler(this::getErrorForCsPlaces))
            .build()
    }

    @Bean
    fun weatherClient(builder: WebClient.Builder, props: ClientProperties): WebClient {
        return builder
            .baseUrl(props.weatherUrl)
            .filter(basicErrorHandler(this::getErrorForWeatherApi))
            .build()
    }


    private fun basicErrorHandler(getError: (clientResponse: ClientResponse) -> Mono<ClientResponse>): ExchangeFilterFunction {
        return ExchangeFilterFunction.ofResponseProcessor {
            val statusCode = it.statusCode()
            return@ofResponseProcessor when {
                statusCode.value() >= 400 -> getError(it)
                else -> Mono.just(it)
            }
        }
    }

    private fun getErrorForCsPlaces(clientResponse: ClientResponse): Mono<ClientResponse> {
        val statusCode = clientResponse.statusCode()

        return clientResponse.bodyToMono(CsClientErrorResponseDto::class.java)
            .mapNotNull { "Ceska sporitelna api returns ${it.status}. More information can be found in the log." }
            .doOnNext { resp -> logger.error { resp } }
            .flatMap {
                when {
                    statusCode.is5xxServerError -> Mono.error(ClientServerException(it, UUID.randomUUID()))
                    else -> Mono.error(ClientException(it, UUID.randomUUID()))
                }
            }
    }

    private fun getErrorForWeatherApi(clientResponse: ClientResponse): Mono<ClientResponse> {
        val statusCode = clientResponse.statusCode()

        return clientResponse
            .bodyToMono(WeatherErrorResponse::class.java)
            .mapNotNull { "Weather api returns ${it.cod} and message ${it.message}" }
            .doOnNext { resp -> logger.error { resp } }
            .flatMap {
                when {
                    statusCode.is5xxServerError -> Mono.error(ClientServerException(it, UUID.randomUUID()))
                    else -> Mono.error(ClientException(it, UUID.randomUUID()))
                }
            }
    }
}