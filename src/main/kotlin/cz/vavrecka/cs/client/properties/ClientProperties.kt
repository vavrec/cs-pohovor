package cz.vavrecka.cs.client.properties

import org.springframework.boot.context.properties.ConfigurationProperties

@ConfigurationProperties("client")
data class ClientProperties(val csPlacesUrl: String, val csPlacesApiKey: String, val weatherUrl: String, val weatherApiKey: String)