package cz.vavrecka.cs.client

import cz.vavrecka.cs.client.model.weather.WeatherResponseDto
import reactor.core.publisher.Mono

interface WeatherClient {
    suspend fun getCurrentWeatherByLocation(lon: Double, lat: Double): WeatherResponseDto
}