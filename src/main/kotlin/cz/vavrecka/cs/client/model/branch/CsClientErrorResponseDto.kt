package cz.vavrecka.cs.client.model.branch

import com.fasterxml.jackson.annotation.JsonProperty

data class CsClientErrorResponseDto(val status : String, val errors : List<CsErrorDto>, @JsonProperty("cz-transactionId") val transactionId: String)