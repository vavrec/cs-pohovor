package cz.vavrecka.cs.client.model.branch

data class LocationDto (val lat : Double, val lng : Double)