package cz.vavrecka.cs.client.model.branch

data class BranchResponseDto(val name: String, val address: String, val city: String, val postCode: String, val stateNote: String, val location: LocationDto)
