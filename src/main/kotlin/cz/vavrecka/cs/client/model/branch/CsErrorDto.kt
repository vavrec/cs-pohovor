package cz.vavrecka.cs.client.model.branch

data class CsErrorDto(val error : String, val scope: String, val parameters: Map<String, Any>)