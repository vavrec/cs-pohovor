package cz.vavrecka.cs.client.model.weather

data class CurrentDto(val weather: List<WeatherDto>)