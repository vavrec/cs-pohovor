package cz.vavrecka.cs.client.model.weather

data class WeatherDto(val description: String)