package cz.vavrecka.cs.client.model.weather

data class WeatherResponseDto(val current: CurrentDto)