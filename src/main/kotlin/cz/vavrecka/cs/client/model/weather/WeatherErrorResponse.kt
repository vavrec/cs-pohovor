package cz.vavrecka.cs.client.model.weather

data class WeatherErrorResponse(val cod: String, val message: String)