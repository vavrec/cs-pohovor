package cz.vavrecka.cs.client.exception

import java.util.UUID

/**
 * Client exception (status code 4xx)
 */
class ClientException(message: String,id: UUID) : WebClientException(message, id)