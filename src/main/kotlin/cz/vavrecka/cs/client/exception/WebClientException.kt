package cz.vavrecka.cs.client.exception

import java.util.*

open class WebClientException(override val message: String, val id: UUID) : RuntimeException(message)