package cz.vavrecka.cs.client.exception

import java.util.*

/**
 * Client server exception (http status 5xx)
 */
class ClientServerException(message: String,id: UUID) : WebClientException(message, id)