package cz.vavrecka.cs.client

import cz.vavrecka.cs.client.model.branch.BranchResponseDto

interface PlaceClient {

    suspend fun getBranchById(id: Long) : BranchResponseDto
}