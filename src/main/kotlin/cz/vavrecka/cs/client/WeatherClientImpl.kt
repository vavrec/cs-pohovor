package cz.vavrecka.cs.client

import cz.vavrecka.cs.client.properties.ClientProperties
import cz.vavrecka.cs.client.exception.ClientServerException
import cz.vavrecka.cs.client.model.weather.WeatherResponseDto
import kotlinx.coroutines.reactive.awaitSingle
import mu.KotlinLogging
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.util.UriBuilder
import reactor.util.retry.Retry
import reactor.util.retry.RetryBackoffSpec
import java.net.URI
import java.time.Duration
import java.util.*

private val logger = KotlinLogging.logger {}

@Component
class WeatherClientImpl(val weatherClient: WebClient, val props: ClientProperties) : WeatherClient {

    override suspend fun getCurrentWeatherByLocation(lon: Double, lat: Double): WeatherResponseDto {
        return weatherClient.get()
            .uri { createUri(it, lon, lat) }
            .retrieve()
            .bodyToMono(WeatherResponseDto::class.java)
            .retryWhen(retryWhen(lon, lat))
            .awaitSingle()
    }

    private fun retryWhen(lon: Double, lat: Double): RetryBackoffSpec {
        // at this moment "max attempts" is just a random number
        return Retry.fixedDelay(2, Duration.ofSeconds(1))
            .filter { it is ClientServerException }
            .doBeforeRetry { logger.info { "Next attempt call get current weather for lon $lon and  lat$lat" } }
            .onRetryExhaustedThrow() { _, signal ->
                ClientServerException("Weather api failed to respond, after max attempts of: ${signal.totalRetries()}", UUID.randomUUID())
            }
    }

    private fun createUri(uriBuilder: UriBuilder, lon: Double, lat: Double): URI {
        return uriBuilder.queryParam("lon", lon)
            .queryParam("lat", lat)
            .queryParam("appid", props.weatherApiKey)
            // at this moment Ceska sporitelna api returns text in czech
            .queryParam("lang", "cz")
            .build()
    }
}