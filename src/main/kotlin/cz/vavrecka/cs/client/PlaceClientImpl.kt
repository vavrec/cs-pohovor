package cz.vavrecka.cs.client

import cz.vavrecka.cs.client.exception.ClientServerException
import cz.vavrecka.cs.client.model.branch.BranchResponseDto
import kotlinx.coroutines.reactive.awaitSingle
import mu.KotlinLogging
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import reactor.util.retry.Retry
import reactor.util.retry.RetryBackoffSpec
import java.time.Duration
import java.util.*

private val logger = KotlinLogging.logger {}

@Component
class PlaceClientImpl(val placesWebClient: WebClient) : PlaceClient {

    override suspend fun getBranchById(id: Long): BranchResponseDto {
        return placesWebClient.get()
            .uri("/branches/{id}", id)
            .retrieve()
            .bodyToMono(BranchResponseDto::class.java)
            // at this moment "max attempts" is just a random number
            .retryWhen(retryWhen(id))
            .awaitSingle()
    }

    private fun retryWhen(id: Long): RetryBackoffSpec {
        // at this moment "max attempts" is just a random number
        return Retry.fixedDelay(2, Duration.ofSeconds(1))
            .filter { it is ClientServerException }
            .doBeforeRetry { logger.info { "Next attempt call get branch by id ($id) cs api" } }
            .onRetryExhaustedThrow() { _, signal ->
                ClientServerException("Places api failed to respond, after max attempts of: ${signal.totalRetries()}", UUID.randomUUID())
            }
    }
}