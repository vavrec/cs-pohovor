package cz.vavrecka.cs

import cz.vavrecka.cs.client.properties.ClientProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(ClientProperties::class)
class CsApplication

fun main(args: Array<String>) {
	runApplication<CsApplication>(*args)
}
