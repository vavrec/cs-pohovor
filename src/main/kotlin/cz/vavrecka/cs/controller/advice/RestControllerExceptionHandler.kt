package cz.vavrecka.cs.controller.advice

import cz.vavrecka.cs.client.exception.ClientException
import cz.vavrecka.cs.client.exception.ClientServerException
import cz.vavrecka.cs.client.exception.WebClientException
import cz.vavrecka.cs.controller.ErrorResponse
import mu.KotlinLogging
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice

private val logger = KotlinLogging.logger {}

@RestControllerAdvice
class RestControllerExceptionHandler {

    @ExceptionHandler(value = [ClientException::class, ClientServerException::class])
    fun clientExceptionHandler(ex: WebClientException): ResponseEntity<ErrorResponse> {
        logger.debug("handling exception::$ex")
        return ResponseEntity.badRequest().body(ErrorResponse(ex.id, ex.message))
    }
}