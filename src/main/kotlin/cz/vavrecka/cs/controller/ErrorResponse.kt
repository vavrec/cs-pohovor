package cz.vavrecka.cs.controller

import java.util.UUID

data class ErrorResponse(val id: UUID, val message: String)