package cz.vavrecka.cs.controller

import cz.vavrecka.cs.service.BranchService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.tags.Tag
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

const val BRANCH_PATH = "/api/v1/branch"

@Tag(name = "Branch")
@ApiResponse(
    responseCode = "400",
    description = "Bad Request",
    content = [
        Content(
            mediaType = MediaType.APPLICATION_JSON_VALUE,
            schema = Schema(implementation = ErrorResponse::class)
        )
    ]
)
@RestController
@RequestMapping(BRANCH_PATH)
class BranchController(val branchService: BranchService) {

    @Operation(
        summary = "Get branch by Id",
        responses = [
            ApiResponse(
                responseCode = "200",
                description = "Get basic information about branch (e.g location, weather etc.)"
            )
        ]
    )
    @GetMapping(path = ["/{id}"], produces = [MediaType.APPLICATION_JSON_VALUE])
    suspend fun getBranchById(@PathVariable id: Long) = ResponseEntity.ok(branchService.getBranchBasicInfoById(id))
}