package cz.vavrecka.cs.service

import cz.vavrecka.cs.model.BranchBasicInfoDto

interface BranchService {

    suspend fun getBranchBasicInfoById(id: Long): BranchBasicInfoDto
}