package cz.vavrecka.cs.service

import cz.vavrecka.cs.client.PlaceClient
import cz.vavrecka.cs.client.WeatherClient
import cz.vavrecka.cs.client.model.branch.BranchResponseDto
import cz.vavrecka.cs.client.model.weather.WeatherResponseDto
import cz.vavrecka.cs.model.BranchBasicInfoDto
import mu.KotlinLogging
import org.springframework.stereotype.Service

private val logger = KotlinLogging.logger {}

@Service
class BranchServiceImpl(val placeClient: PlaceClient, val weatherClient: WeatherClient) : BranchService {

    override suspend fun getBranchBasicInfoById(id: Long): BranchBasicInfoDto {

        val branch = placeClient.getBranchById(id)
        logger.info("Received Branch (id: $id) $branch")

        val weather = weatherClient.getCurrentWeatherByLocation(lat = branch.location.lat, lon = branch.location.lng)
        logger.info("Received weather (lat ${branch.location.lat} , lng ${branch.location.lng}):  $weather")

        return mapToBranchBasicInfo(branch, weather)
    }


    private fun mapToBranchBasicInfo(branch: BranchResponseDto, weather: WeatherResponseDto): BranchBasicInfoDto {
        val descriptions = weather.current.weather.map { it.description }

        return BranchBasicInfoDto(
            name = branch.name, address = branch.address, city = branch.city, postCode = branch.postCode, stateNote = branch.stateNote,
            weatherDescriptions = descriptions
        )
    }
}