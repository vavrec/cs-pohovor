package cz.vavrecka.cs.model

data class BranchBasicInfoDto(val name: String, val address: String, val city: String, val postCode: String, val stateNote: String, val weatherDescriptions: List<String>)